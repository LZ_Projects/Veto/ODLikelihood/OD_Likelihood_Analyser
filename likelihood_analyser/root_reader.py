import uproot as up
import numpy as np


def load_histogram(file, hist_path):
    up_file = up.open(file)
    weights, bins = up_file[hist_path].to_numpy()

    return weights, bins


def load_likelihoods(file, hist_paths):

    up_file = up.open(file)
    likelihoods = {}
    for hist in hist_paths:
        weights, bins = up_file[hist].to_numpy()
        likelihoods[hist] = weights

    return likelihoods, bins
